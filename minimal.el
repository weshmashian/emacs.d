(require 'package)

(customize-set-variable 'package-enable-at-startup nil)

(customize-set-variable 'package-archives
			'(("welpa" . "http://weshmashian.gitlab.io/welpa/")
			  ("melpa" . "http://melpa.org/packages/")
			  ("gnu"   . "http://elpa.gnu.org/packages/")
			  ("nongnu" . "http://elpa.nongnu.org/nongnu/")))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(use-package evil-leader
  :custom
  (evil-want-integration t)
  (evil-want-keybinding nil)
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "0" 'delete-window
    "1" 'delete-other-windows
    "2" 'split-window-below
    "3" 'split-window-right
    "5" 'make-frame-command
    "b" 'ibuffer
    "d" 'dired
    "f" 'find-file
    "w o" 'other-window)
  (global-evil-leader-mode))

(use-package evil
  :after (evil-leader)
  :custom
  (evil-toggle-key "C-x C-z")
  (evil-want-C-i-jump nil)
  :config (evil-mode 1))

(ignore-errors
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1))

(column-number-mode)

(defalias 'yes-or-no-p 'y-or-n-p)

;; Enable some features disabled by default
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(customize-set-variable 'inhibit-startup-screen t)
(customize-set-variable 'show-trailing-whitespace nil)
(customize-set-variable 'major-mode 'text-mode)
(customize-set-variable 'use-package-always-ensure t)
(customize-set-variable 'custom-safe-themes t)
(customize-set-variable 'browse-url-browser-function 'browse-url-generic)
(customize-set-variable 'browse-url-generic-program "google-chrome")
(customize-set-variable 'calendar-date-style 'iso)
(customize-set-variable 'calendar-week-start-day 1)
(customize-set-variable 'fill-column 100)
(customize-set-variable 'save-interprogram-paste-before-kill t)
(customize-set-variable 'sentence-end-double-space nil)

;; Load host-specific settings
(setq custom-file (expand-file-name (concat user-emacs-directory "host_testing.el")))
(when (file-exists-p custom-file)
  (load custom-file))
