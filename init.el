;; Speed up Emacs loading.
;; Profiling startup times: emacs -Q -l ~/git/profile-dotemacs/profile-dotemacs.el -f profile-dotemacs
(setq gc-cons-threshold 64000000)
(run-with-idle-timer 5 nil
                     (lambda ()
                       (setq gc-cons-threshold 800000)
                       (message "gc-cons-threshold restored to %S"
                                gc-cons-threshold)))

;; Deal with packages only
(require 'package)

(customize-set-variable 'package-enable-at-startup nil)

(customize-set-variable 'package-archives
                        '(("welpa" . "http://weshmashian.gitlab.io/welpa/")
                          ("melpa" . "http://melpa.org/packages/")
                          ("gnu"   . "http://elpa.gnu.org/packages/")
                          ("nongnu" . "http://elpa.nongnu.org/nongnu/")))
(package-initialize)

;; Bootstrap use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(use-package general-settings
  :ensure nil
  :load-path "site-lisp/general-settings")

;; Just refresh list of packages if last update was over 2 days ago. Will not run after bootstrap.
;; (general/refresh-packages)

(use-package leuven-theme
  :config (load-theme 'leuven))

;; (use-package calfw)
;; (use-package calfw-org :after (org))
(use-package company
  :config (add-hook 'prog-mode-hook 'company-mode-on))
(use-package company-jedi
  :config (add-to-list 'company-backends 'company-jedi))
(use-package company-go
  :config (add-to-list 'company-backends 'company-go))
  ;; https://github.com/golang/tools/blob/master/gopls/doc/emacs.md
  ;; for setting up `gopls' instead of `gocode'
  ;; :custom (company-go-gocode-command (expand-file-name "~/go/bin/gocode")))
(use-package copy-as-format)
(use-package dockerfile-mode)
(use-package flycheck
  :defer
  :custom (flycheck-disabled-checkers '(emacs-lisp-checkdoc)))
(use-package git-link)
(use-package ghub :disabled)
(use-package glab :disabled)
(use-package htmlize)
(use-package ibuffer-vc)
(use-package ido-vertical-mode
  :config
  (ido-mode 1)
  (ido-vertical-mode 1)
  :custom
  (ido-vertical-define-keys 'C-n-C-p-up-down-left-right))
(use-package imenu-anywhere
  :custom (imenu-anywhere-buffer-list-function 'general/imenu-anywhere-list-buffers))
(use-package indent-tools)
(use-package json-mode)
(use-package ledger-mode
  :mode "\\.ledger\\'")
(use-package lua-mode
  :mode "\\.lua\\'")
(use-package markdown-mode
  :mode "\\.md\\'")
(use-package mustache-mode)
(use-package nginx-mode)
(use-package php-mode
  :mode "\\.php\\'")
(use-package sensitive-mode
  :mode "\\.gpg\\'")
(use-package simple-kanban
  :after (org)
  :defer)
(use-package systemd)
(use-package toml-mode)
(use-package virtualenvwrapper :defer)
(use-package which-key
  :config (which-key-mode))
(use-package yaml-mode
  :config (add-hook 'yaml-mode-hook 'general/linum-whitespace-mode))

(use-package evil-leader
  :custom
  (evil-want-integration t)
  (evil-want-keybinding nil)
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    "0" 'delete-window
    "1" 'delete-other-windows
    "2" 'split-window-below
    "3" 'split-window-right
    "5" 'make-frame-command
    "b" 'ibuffer
    "B" 'projectile-ibuffer
    "d" 'dired
    "f" 'find-file
    "c c" 'cfw:open-org-calendar
    "g b" 'magit-branch
    "g B" 'magit-blame
    "g d" 'magit-dispatch
    "g F" 'magit-pull
    "g f" 'magit-fetch
    "g g" 'magit-status
    "g h" 'git-link-homepage
    "g l" 'magit-log
    "g L" 'git-link
    "g o" 'magit-reset
    "g p" 'magit-push
    "g r" 'magit-rebase
    "g /" 'vc-git-grep
    "g s m" 'smerge-keep-mine
    "g s n" 'smerge-next
    "g s o" 'smerge-keep-other
    "g s p" 'smerge-prev
    "i i" 'imenu-anywhere
    "i c" 'general/ispell-change-dictionary
    "p b" 'projectile-ibuffer
    "p d" 'projectile-find-dir
    "p D" 'projectile-dired
    "p e" 'projectile-recentf
    "p f" 'projectile-find-file
    "p F" 'projectile-find-file-other-window
    "p s" 'projectile-switch-project
    "w o" 'other-window)
  (global-evil-leader-mode))

(use-package evil
  :after (evil-leader)
  :custom
  (evil-toggle-key "C-x C-z")
  (evil-want-C-i-jump nil)
  :config (evil-mode 1))

(use-package evil-quickscope
  :config (global-evil-quickscope-mode 1))

(use-package evil-surround
  :config (global-evil-surround-mode))

(use-package org
  :pin nongnu
  :ensure org-contrib
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture)
         ("C-c l" . org-store-link))
  :config
  (set-face-attribute 'org-agenda-dimmed-todo-face t :foreground "firebrick")
  (add-hook 'org-mode-hook 'flyspell-mode)
  :custom
  (org-ascii-text-width 100)
  (org-agenda-files (list "~/org"))
  (org-agenda-include-diary nil)
  (org-agenda-span 10)
  (org-archive-location "archive/%s::")
  (org-babel-load-languages '((emacs-lisp . t)
                              (ledger . t)
                              (shell . t)
                              (python . t)
                              (plantuml . t)))
  (org-babel-process-comment-text (lambda (code) (org-export-string-as code 'ascii t)))
  (org-capture-templates
   `(("L" "Protocol Link" entry
      (file+headline "" "Links")
      "* %?\n%(progn (setq kk/delete-frame-after-capture 1) \"\"):PROPERTIES:\n:CREATED: %u\n:END:\n\n[[%:link][%:description]] ")
     ("n" "Note" entry
      (file+headline "" "Notes")
      "* %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n\n%a")
     ("p" "Protocol" entry
      (file+headline "" "Links")
      "* %^{Title}\n:PROPERTIES:\n:CREATED: %u\n:END:\nSource: %c\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%? %(progn (setq kk/delete-frame-after-capture 1) \"\")")
     ("t" "Task" entry
      (file+headline "" "Tasks")
      "* TODO %^{Title}\n:PROPERTIES:\n:CREATED: %u\n:END:\n\n%a\n%?")))
  (org-default-notes-file "~/org/notes.org")
  (org-duration-format (quote h:mm))
  (org-enforce-todo-dependencies t)
  (org-expiry-inactive-timestamps t)
  (org-export-backends '(html ascii md confluence latex odt))
  (org-export-with-drawers nil)
  (org-export-with-sub-superscripts '{})
  (org-extend-today-until 3)
  (org-footnote-auto-ajdust t)
  (org-html-validation-link nil)
  (org-insert-heading-respect-content t)
  (org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)
  (org-log-done 'time)
  (org-log-into-drawer t)
  (org-modules '(org-bbdb org-bibtex org-docview org-gnus org-habit org-id org-info org-mhe org-rmail org-w3m))
  (org-pantomime-use-property-inheritance t)
  (org-plantuml-jar-path (expand-file-name (concat user-emacs-directory "contrib/plantuml.jar")))
  (org-refile-use-outline-path 'file)
  (org-refile-targets '((nil :maxlevel . 2) (org-agenda-files :maxlevel . 2)))
  (org-src-fontify-natively t)
  (org-src-preserve-indentation t)
  (org-tags-exclude-from-inheritance '("prj")))

(use-package org-helpers
  :after (org)
  :ensure nil
  :load-path "site-lisp/org-helpers")

(use-package org-custom-id
  :after (org-helpers))

(use-package ox-gfm
  :after (org-helpers))

(use-package dired-subtree
  :ensure t
  :after dired
  :custom (dired-subtree-use-backgrounds nil)
  :config
  (bind-key "<tab>" #'dired-subtree-toggle dired-mode-map)
  (bind-key "<backtab>" #'dired-subtree-cycle dired-mode-map))

(use-package magit
  :bind ("C-x g" . magit-status)
  :after (evil-leader)
  :config
  (magit-add-section-hook 'git-commit-setup-hook 'git-commit-turn-on-flyspell)
  (magit-add-section-hook 'git-commit-setup-hook '(lambda () (setq fill-column 72)))
  (evil-leader/set-key-for-mode 'magit-log-mode "g l" 'git-link-commit)
  (evil-leader/set-key-for-mode 'magit-revision-mode "g l" 'git-link-commit))

(use-package evil-collection
  :after evil
  :ensure t
  :config (evil-collection-init))

(use-package puppet-mode
  :mode "\\.pp\\'"
  :config (add-hook 'puppet-mode-hook (lambda() (set-fill-column 72)))
  :custom
  (puppet-lint-command "puppet-lint --log-format='%{path}:%{line}:%{kind} %{message}'")
  (puppet-indent-level 4))

(use-package recentf
  :config (recentf-mode t))

(use-package projectile
  :after (recentf)
  :config (projectile-mode))

(use-package eglot)

(use-package go-mode
  :mode "\\.go\\'"
  :config (add-hook 'go-mode-hook 'eglot-ensure))
(use-package go-eldoc
  :after (go-mode)
  :config (add-hook 'go-mode-hook 'go-eldoc-setup))
(use-package go-guru
  :config (add-hook 'go-mode-hook 'go-guru-hl-identifier-mode)
  :after (go-mode))

(use-package occur-x
  :config (add-hook 'occur-mode-hook 'turn-on-occur-x-mode))

(use-package page-break-lines
  :config (global-page-break-lines-mode))

(use-package spaceline)
(use-package spaceline-config
  :ensure spaceline
  :custom
  (mode-line-format '("%e" (:eval (spaceline-ml-main))))
  (spaceline-highlight-face-func 'spaceline-highlight-face-modified)
  :config (spaceline-install
            ;; Left
            `((buffer-modified :face highlight-face :priority 9)
              (((projectile-root :when active) buffer-id) :separator "/" :priority 9)
              (version-control :when active)
              (org-clock :when active :face highlight-face)
              ((flycheck-error flycheck-warning flycheck-info) :when active :priority 3)
              process
              (global :when active))
            ;; Other left
            `(remote-host
              (major-mode)
              ((buffer-position line-column) :separator " | " :priority 9)
              (buffer-size)
              (evil-state))))

(use-package flycheck-yamllint
  :if (executable-find "yamllint")
  :after (flycheck)
  :config (add-hook 'flycheck-mode-hook 'flycheck-yamllint-setup))

(use-package typescript-mode)

;; Load host-specific settings
(setq custom-file (expand-file-name (concat user-emacs-directory "host_" system-name ".el")))
(when (file-exists-p custom-file)
  (load custom-file))
