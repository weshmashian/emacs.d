;;; general-settings.el --- Package holding generic options

;; Copyright (C) 2016-2018  Marko Crnic

;; Author: Marko Crnic <macrnic@gmail.com>
;; Keywords: lisp
;; Version: 0.0.2

;;; Commentary:

;;; Code:

(eval-when-compile
  (declare-function ibuffer-current-filter-groups-with-position "ibuffer")
  (defvar ibuffer-filter-groups "ibuf-ext")
  (defvar evil-leader--mode-maps)
  (defvar evil-leader--default-map)
  (defvar ibuffer-hidden-filter-groups)
  (defvar gnus-extra-headers)
  (defvar display-line-numbers-type))

;; Basic global stuff
(ignore-errors
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1))

;; Set GUI windows size
(add-to-list 'default-frame-alist '(height . 35))
(add-to-list 'default-frame-alist '(width . 110))

(column-number-mode)

(show-paren-mode)
(customize-set-variable 'show-paren-delay 0 "Customized in general-settings.el")

(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(add-hook 'grep-mode-hook #'(lambda () (toggle-truncate-lines 1)))

(xterm-mouse-mode t)
(global-set-key (kbd "<mouse-4>") (lambda () (interactive) (scroll-down-line 5)))
(global-set-key (kbd "<mouse-5>") (lambda () (interactive) (scroll-up-line 5)))

(defalias 'yes-or-no-p 'y-or-n-p)

;; Enable some features disabled by default
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;; Important faces
(set-face-attribute 'trailing-whitespace nil :background "red")

(customize-set-variable 'frame-background-mode 'light "Customized in general-settings.el")
(customize-set-variable 'inhibit-startup-screen t "Customized in general-settings.el")
(customize-set-variable 'show-trailing-whitespace nil "Customized in general-settings.el")
(customize-set-variable 'major-mode 'text-mode "Customized in general-settings.el")
(customize-set-variable 'use-package-always-ensure t "Customized in general-settings.el")
(customize-set-variable 'custom-safe-themes t "Customized in general-settings.el")
(customize-set-variable 'browse-url-browser-function 'browse-url-generic "Customized in general-settings.el")
(customize-set-variable 'browse-url-generic-program "google-chrome" "Customized in general-settings.el")
(customize-set-variable 'calendar-date-style 'iso "Customized in general-settings.el")
(customize-set-variable 'calendar-week-start-day 1 "Customized in general-settings.el")
(customize-set-variable 'fill-column 100 "Customized in general-settings.el")
(customize-set-variable 'save-interprogram-paste-before-kill t "Customized in general-settings.el")
(customize-set-variable 'sentence-end-double-space nil "Customized in general-settings.el")
(customize-set-variable 'indent-tabs-mode nil "Customized in general-settings.el")

;; Backup options
(customize-set-variable 'backup-directory-alist `(("." . ,(concat user-emacs-directory "backup"))) "Customized in general-settings.el")
(customize-set-variable 'trash-directory (concat user-emacs-directory "trash") "Customized in general-settings.el")
(customize-set-variable 'make-backup-files t "Customized in general-settings.el")               ; backup of a file the first time it is saved.
(customize-set-variable 'backup-by-copying t "Customized in general-settings.el")               ; don't clobber symlinks
(customize-set-variable 'version-control t "Customized in general-settings.el")                 ; version numbers for backup files
(customize-set-variable 'delete-old-versions t "Customized in general-settings.el")             ; delete excess backup files silently
(customize-set-variable 'delete-by-moving-to-trash t "Customized in general-settings.el")
(customize-set-variable 'kept-old-versions 6 "Customized in general-settings.el")               ; oldest versions to keep when a new numbered backup is made (default: 2)
(customize-set-variable 'kept-new-versions 9 "Customized in general-settings.el")               ; newest versions to keep when a new numbered backup is made (default: 2)
(customize-set-variable 'auto-save-default t "Customized in general-settings.el")               ; auto-save every buffer that visits a file
(customize-set-variable 'auto-save-timeout 20 "Customized in general-settings.el")              ; number of seconds idle time before auto-save (default: 30)
(customize-set-variable 'auto-save-interval 200 "Customized in general-settings.el")            ; number of keystrokes between auto-saves (default: 300)

;; Ibuffer
(use-package ibuffer
  :custom
  (ibuffer-show-empty-filter-groups nil)
  (ibuffer-saved-filter-groups
   (quote
    (("filter1"
      ("star"
       (name . "*"))
      ("dired"
       (used-mode . dired-mode))
      ("org"
       (used-mode . org-mode))))))
  (ibuffer-saved-filters
   (quote
    (("gnus"
      ((or
	(mode . message-mode)
	(mode . mail-mode)
	(mode . gnus-group-mode)
	(mode . gnus-summary-mode)
	(mode . gnus-article-mode))))
     ("programming"
      ((or
	(mode . emacs-lisp-mode)
	(mode . cperl-mode)
	(mode . c-mode)
	(mode . java-mode)
	(mode . idl-mode)
	(mode . puppet-mode)
	(mode . lisp-mode))))))))

(declare-function ibuffer-update "ibuffer")
(declare-function ibuffer-jump-to-buffer "ibuffer")
;; Taken from http://acidwords.com/posts/2016-06-18-collapsing-all-filter-groups-in-ibuffer.html
(defun general/ibuffer-collapse-all-filter-groups ()
  "Collapse all filter groups at once."
  (interactive)
  (setq ibuffer-filter-groups (ibuffer-vc-generate-filter-groups-by-vc-root))
  (setq ibuffer-hidden-filter-groups
	(mapcar #'car (ibuffer-current-filter-groups-with-position)))
  (ibuffer-update nil t)
  (ibuffer-jump-to-buffer (buffer-name (other-buffer))))

(add-hook 'ibuffer-hook 'general/ibuffer-collapse-all-filter-groups)

;; Mail
(use-package smtpmail
  :custom
  (message-citation-line-format "On %a,%b %d %Y, %N wrote:")
  (message-citation-line-function 'message-insert-formatted-citation-line)
  (message-kill-buffer-on-exit t)
  (message-send-function 'smtpmail-send-it)
  (send-mail-function 'smtpmail-send-it)
  (smtpmail-local-domain nil))

;; Gnus
(customize-set-variable 'gnus-select-method '(nntp "news.gmane.org") "Customized in general-settings.el")
(customize-set-variable 'gnus-read-active-file nil "Customized in general-settings.el")
(customize-set-variable 'gnus-use-adaptive-scoring t "Customized in general-settings.el")
(customize-set-variable 'gnus-summary-expunge-below -10 "Customized in general-settings.el")
(customize-set-variable 'gnus-adaptive-word-no-group-words t "Customized in general-settings.el")
(customize-set-variable 'gnus-home-score-file 'gnus-hierarchial-home-score-file "Customized in general-settings.el")
(customize-set-variable 'gnus-home-adapt-file 'gnus-hierarchial-home-score-file "Customized in general-settings.el")
(customize-set-variable 'gnus-extra-headers '(To Cc Keywords Gcc Newsgroups X-Spam-Flag X-Barracuda-Spam-Flag) "Customized in general-settings.el")
(customize-set-variable 'nnmail-extra-headers gnus-extra-headers "Customized in general-settings.el")

(require 'spam)
(spam-initialize 'spam-use-gmane-xref 'spam-use-regex-headers)
(customize-set-variable 'spam-regex-headers-spam '("^X-\\(Barracuda-\\)?Spam-Flag: YES") "Customized in general-settings.el")

;; Calendars
(defvar holiday-croatian-holidays
  '((holiday-fixed 1 1 "Nova godina")
    (holiday-fixed 1 6 "Sveta tri kralja")
    (holiday-easter-etc -2 "Veliki Petak")
    (holiday-easter-etc 0 "Uskrs")
    (holiday-easter-etc 1 "Uskršnji ponedjeljak")
    (holiday-fixed 5 1 "Praznik rada")
    (holiday-easter-etc +60 "Tijelovo")
    (holiday-fixed 5 30 "Dan državnosti")
    (holiday-fixed 6 22 "Dan antifašističke borbe")
    (holiday-fixed 8 5 "Dan domovinske zahvalnosti")
    (holiday-fixed 8 15 "Velika Gospa")
    (holiday-fixed 10 8 "Dan neovisnosti")
    (holiday-fixed 11 1 "Dan svih svetih")
    (holiday-fixed 11 18 "Dan sjećanja na žrtve Domovinskog rata")
    (holiday-fixed 12 25 "Božić")
    (holiday-fixed 12 26 "Sveti Stjepan"))
  "Croatian holidays.")

(defvar holiday-french-holidays
  '((holiday-fixed 1 1 "Jour de l'an")
	(holiday-fixed 1 6 "Épiphanie")
	(holiday-fixed 5 1 "Fête du travail")
	(holiday-fixed 5 8 "Commémoration de la capitulation de l'Allemagne en 1945")
	(holiday-fixed 7 14 "Fête nationale - Prise de la Bastille")
	(holiday-fixed 8 15 "Assomption (Religieux)")
	(holiday-fixed 11 11 "Armistice de 1918")
	(holiday-fixed 11 1 "Toussaint")
	(holiday-fixed 11 2 "Commémoration des fidèles défunts")
	(holiday-fixed 12 25 "Noël")
        ;; fetes a date variable
	(holiday-easter-etc 0 "Pâques")
        (holiday-easter-etc 1 "Lundi de Pâques")
        (holiday-easter-etc 39 "Ascension")
        (holiday-easter-etc 49 "Pentecôte"))
  "French holidays.")

(use-package netherlands-holidays)

(use-package calendar
  :custom
  (calendar-holidays (append holiday-general-holidays holiday-solar-holidays holiday-christian-holidays holiday-netherlands-holidays holiday-french-holidays holiday-croatian-holidays)))

(require 'holidays)
(customize-set-variable 'holiday-bahai-holidays nil "Customized in general-settings.el")
(customize-set-variable 'holiday-hebrew-holidays nil "Customized in general-settings.el")
(customize-set-variable 'holiday-islamic-holidays nil "Customized in general-settings.el")
(customize-set-variable 'holiday-local-holidays holiday-croatian-holidays "Customized in general-settings.el")

;; Flyspell
(customize-set-variable 'flyspell-use-meta-tab nil "Customized in general-settings.el")

;; Ido mode
(customize-set-variable 'ido-enable-flex-matching t "Customized in general-settings.el")
(customize-set-variable 'ido-ignore-extensions t "Customized in general-settings.el")
(customize-set-variable 'ido-file-extensions-order '(".org" ".sh" ".py" ".el" ".txt") "Customized in general-settings.el")
(add-to-list 'completion-ignored-extensions "#" t)

;; Create directories
(dolist (dir backup-directory-alist trash-directory)
  (if (not (file-exists-p (cdr dir)))
      (make-directory (cdr dir) t)))

;; Server stuff
(require 'server)
(unless (or (daemonp) (server-running-p)) (server-start))

(defun stop-emacs-daemon ()
  "Gracefully shut down running Emacs daemon."
  (interactive)
  (if (daemonp)
      (save-buffers-kill-emacs)
      (save-buffers-kill-terminal)))

(defun general/linum-whitespace-mode ()
  "Set up common settings for `prog-mode' and derivatives."
  (setq display-line-numbers-type 'relative)
  (display-line-numbers-mode)
  (flycheck-mode)
  (setq show-trailing-whitespace t))

(add-hook 'prog-mode-hook 'general/linum-whitespace-mode)

;; Highlight keywords.
(add-hook 'prog-mode-hook
          (lambda ()
            (font-lock-add-keywords
             nil '(("\\<\\(FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t)))))

;; Other stuff
(defun general/refresh-packages ()
  "Refresh package archives if last update was over 2 days ago."
  (let* ((repo (caar package-archives))
         (archive (concat package-user-dir "/archives/" repo "/archive-contents"))
         (mtime))
    (when
        (or (not (file-exists-p archive))
            (> (- (car (current-time)) (caar (nthcdr 5 (file-attributes archive 'string)))) 1))
      (package-refresh-contents))))

(defun general/evil-leader-unset-key (key &optional mode)
  "Remove KEY from MODE.

If MODE is nil, remove from default keymap."
  (define-key
    (if mode
	(cdr (assq mode evil-leader--mode-maps))
      evil-leader--default-map)
    key nil))

(defun general/imenu-anywhere-list-buffers ()
  "Filter out temporary and Magit buffers.

Used with `imenu-anywhere-buffer-list-function'."
  (let (buflist)
    (dolist (buffer (buffer-list) buflist)
      (cond
       ((string= "*" (substring (buffer-name buffer) 0 1)) nil)
       ((string= "magit" (substring (symbol-name (buffer-local-value 'major-mode buffer)) 0 5)) nil)
       (t (setq buflist (cons buffer buflist)))))))

;; Spelling
(require 'ring)
(require 'ispell)

(defvar general/ispell-dictionaries (ring-convert-sequence-to-ring '(nil "hr"))
  "List of ispell dictionaries to use. `nil' is default dictionary.")

(defun general/ispell-change-dictionary ()
  "Cycle and set dictionaries."
  (interactive)
  (ispell-change-dictionary (ring-next general/ispell-dictionaries ispell-current-dictionary)))

(provide 'general-settings)
;;; general-settings.el ends here
