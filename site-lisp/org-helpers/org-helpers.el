;;; org-helpers.el --- Misc stuff helping with using org-mode

;; Copyright (C) 2016-2018,2024 Marko Crnic

;; Author: Marko Crnic <macrnic@gmail.com>
;; Keywords: lisp
;; Version: 0.0.2

;;; Commentary:
;; Code used to make life with Org more fun.

;;; Code:
(require 'evil)
(require 'org-agenda)
(require 'org-crypt)
(require 'org-protocol)
(require 'org-secretary)
(require 'ox-confluence)
(require 'ox-md)
(require 'message)

(use-package ob-http
  :custom (ob-http:remove-cr t)
  :config (add-to-list 'org-babel-load-languages '(http . t)))

(use-package org-pantomime)

(declare-function mu4e~request-contacts "mu4e")
(declare-function mu4e~compose-setup-completion "mu4e")
(declare-function mu4e-context-switch "mu4e")

(org-crypt-use-before-save-magic)
;; (org-expiry-insinuate)

;; Move around org-agenda using j/k
(evil-define-key 'emacs org-agenda-mode-map
  "j" 'org-agenda-next-line
  "k" 'org-agenda-previous-line
  (kbd "C-j") 'org-agenda-goto-date
  "n" 'org-agenda-capture)

(evil-define-key 'normal evil-org-mode-map
  "$" 'org-end-of-line
  "^" 'org-beginning-of-line
  "<" 'org-metaleft
  ">" 'org-metaright
  (kbd "<tab>") 'org-cycle)

;; Make RET useful in normal mode
(evil-define-key 'normal org-mode-map
  (kbd "<RET>") 'org-open-at-point)

(when (featurep 'evil-leader)
  (evil-leader/set-key-for-mode 'org-mode "o l l" 'org-insert-link)
  (evil-leader/set-key-for-mode 'org-mode "o l c" 'org-custom-id-insert-link)
  (evil-leader/set-key-for-mode 'org-mode "o l C" 'org-custom-id-get-create)
  (evil-leader/set-key-for-mode 'org-mode "o x" 'org-cut-subtree)
  (evil-leader/set-key-for-mode 'org-mode "o p" 'org-paste-subtree)
  (evil-leader/set-key-for-mode 'org-mode "o e" 'org-export-dispatch)
  (evil-leader/set-key-for-mode 'org-mode "o t" 'org-babel-tangle)
  (evil-leader/set-key-for-mode 'org-mode "o g" 'org-goto))

(defadvice org-goto (around make-it-evil activate)
  "Use Emacs keys in org-goto interface."
  (let ((orig-state evil-state)
        (evil-emacs-state-modes (cons 'org-mode evil-emacs-state-modes)))
    ad-do-it
    (evil-change-state orig-state)))

(defun org-archive-done-tasks ()
  "Archive all closed second-level Tasks older than 3 months."
  (interactive)
  (org-map-entries
   (lambda ()
     (org-archive-subtree)
     (setq org-map-continue-from (outline-previous-heading)))
   "CATEGORY={tasks}+LEVEL=2+CLOSED<\"<-3m>\""))

(defun org-archive-done-projects ()
  "Archive all closed second-level Projects older than 3 months."
  (interactive)
  (org-map-entries
   (lambda ()
     (org-archive-subtree)
     (setq org-map-continue-from (outline-previous-heading)))
   "CATEGORY={projects}+LEVEL=2+CLOSED<\"<-3m>\""))

(defun org-archive-done-stuff ()
  "Archive Tasks and Projects"
  (interactive)
  (org-archive-done-tasks)
  (org-archive-done-projects))

;; Following two defuns were taken from https://github.com/howardabrams/dot-files/blob/master/elisp/ox-confluence.el
(defun org-confluence-unfill-string (s)
  "Remove initial and trailing whitespace and newlines throughout string, S."
  (org-trim (replace-regexp-in-string "[\n\r\t ]+" " " s)))

(defun org-confluence-paragraph-string (s)
  "Remove initial and trailing whitespace and newlines throughout string, S, except for paragraph separators."
  (let ((paragraphs (split-string s "\n[ \t]*\n")))
    (string-join
     (mapcar 'org-confluence-unfill-string paragraphs)
     "\n\n")))

(defun mc/org-confluence--block (language theme contents)
  "When exporting LANGUAGE completely ignore THEME but process CONTENTS."
  (concat "\{code:"
          (when language (format "language=%s" language))
          "}\n"
          contents
          "\{code\}\n"))

(advice-add 'org-confluence-paragraph :filter-return #'org-confluence-paragraph-string)
(advice-add 'org-confluence--block :override #'mc/org-confluence--block)

;; kk/ functions were taken from https://github.com/sprig/org-capture-extension
;; Kill the frame if one was created for the capture
(defvar kk/delete-frame-after-capture 0 "Whether to delete the last frame after the current capture.")

(defun kk/delete-frame-if-neccessary (&rest r)
  "Delete frames if neccessary. R is optional and does stuff."
  (cond
   ((= kk/delete-frame-after-capture 0) nil)
   ((> kk/delete-frame-after-capture 1)
    (setq kk/delete-frame-after-capture (- kk/delete-frame-after-capture 1)))
   (t
    (setq kk/delete-frame-after-capture 0)
    (delete-frame))))

(advice-add 'org-capture-finalize :after 'kk/delete-frame-if-neccessary)
(advice-add 'org-capture-kill :after 'kk/delete-frame-if-neccessary)
(advice-add 'org-capture-refile :after 'kk/delete-frame-if-neccessary)

(add-hook 'org-capture-mode-hook 'delete-other-windows)

;; Make `org-pantomime-subtree' easier to use.
(defun mc/org-pantomime-compose (body fmt file &optional to subject headers opts)
  "Load up mu4e address completion and jump to To: field.

See `org-pantomime-compose` for description of BODY, FMT, FILE,
TO, SUBJECT and HEADERS. I have no idea where OPTS came from."
  (when (featurep 'mu4e)
    (mu4e~request-contacts)
    (mu4e~compose-setup-completion)
    (mu4e-context-switch))
  (message-goto-to))

(advice-add 'org-pantomime-compose :after #'mc/org-pantomime-compose)

;; Provide autocompletion for id: links
(defun org-id-complete-link (&optional arg)
  "Create an id: link using completion"
  (concat "id:" (org-id-get-with-outline-path-completion)))

(org-link-set-parameters "id" :complete 'org-id-complete-link)

;; Needed on first run after full package install. Quitting emacs tries to call this and we're
;; loading orgmode from other sources.
(unless (fboundp 'org-clocking-buffer)
  (defun org-clocking-buffer (&rest _)))

;; https://emacs.stackexchange.com/questions/35751/how-to-add-a-created-field-to-any-todo-task
(defun my/log-todo-creation-date (&rest ignore)
  "Log TODO creation time in the property drawer under the key 'CREATED'."
  (when (and (org-get-todo-state)
             (not (org-entry-get nil "CREATED")))
    (org-entry-put nil "CREATED" (format-time-string (cdr org-time-stamp-formats)))))

(advice-add 'org-insert-todo-heading :after #'my/log-todo-creation-date)
(advice-add 'org-insert-todo-heading-respect-content :after #'my/log-todo-creation-date)
(advice-add 'org-insert-todo-subheading :after #'my/log-todo-creation-date)
(add-hook 'org-after-todo-state-change-hook #'my/log-todo-creation-date)

(provide 'org-helpers)
;;; org-helpers.el ends here
